import itertools

from .base import HumanBase


class HumansIterator:
    """ Iterator class """

    def __init__(self, humans):
        # Humans object reference
        self.humans = humans
        # member variable to keep track of current index
        self._index = 0

    def __next__(self):
        """ Returns the next value from team object's lists """
        if self._index < (len(self.humans)):
            result = self.humans[self._index]
            self._index += 1
            return result
        # End of Iteration
        raise StopIteration


class Humans(list):
    """
    Contains List of Humans and overrides the __iter__() function.
    """

    def __init__(self, humans=None):
        super().__init__()
        if humans is not None:
            try:
                for obj in humans:
                    self.append(obj)
            except TypeError:
                raise TypeError("Must be an iterable")

    def add(self, obj: HumanBase):
        if type(obj) is not HumanBase:
            raise TypeError("Only HumanBase accepted")
        return self.append(obj)

    def __add__(self, other):
        try:
            self.append(other)
        except TypeError:
            # try to loop on an iterable, add() raises TypeError if not Human:
            for obj in iter(other):
                self.add(obj)
        return self

    def __iter__(self):
        """ Creates an Iterator object passing self = list: """
        return HumansIterator(self)

    def flattened(self):
        return [
            a for a in itertools.chain.from_iterable([h.infos() for h in self])
        ]
