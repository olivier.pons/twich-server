from enum import Enum, auto


class HumanState(Enum):
    AT_HOME = auto()
    TO_WORK = auto()
    AT_WORK = auto()
    TO_HOME = auto()
