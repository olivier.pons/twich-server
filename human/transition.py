from human.state import HumanState


class Transition:
    def __init__(self, transition, duration: float, position: tuple, final_state: HumanState):
        self.transition = transition
        self.duration = duration
        self.position = position
        self.final_state = final_state
