from datetime import datetime
from random import choice

from .state import HumanState as St
from .transition import Transition


class HumanBase:
    def __init__(self, uid: int, obj_type: int, x: float, y: float, z: float):
        self.uid = uid
        self.obj_type = obj_type
        self.x = x
        self.y = y
        self.z = z
        self.state: St = St.AT_HOME
        self.transitions = {
            # (time start, time end): (transition, duration, pos., final state)
            ((8, 00), (8, 30)): Transition(
                transition=St.TO_WORK, duration=15, position=(50, 0, 50),
                final_state=St.AT_WORK
            ),
            ((17, 45), (18, 30)): Transition(
                transition=St.TO_HOME, duration=15, position=(50, 0, 50),
                final_state=St.AT_HOME
            ),
        }
        self.transition = None

    @staticmethod
    def scaled(
            date_time: datetime, hh: int, mn: int, scale: float
    ) -> float:
        return date_time.replace(hour=hh, minute=mn).timestamp() * scale

    def next_transition(self, dt: datetime, scale: float = 1.0):
        time_scaled = dt.timestamp() * scale
        try:
            return choice([
                # s_xx = start, e_xx = end:
                v for ((s_hh, s_mn), (e_hh, e_mn)), v in
                self.transitions.items()
                if self.scaled(dt, s_hh, s_mn, scale) <
                time_scaled < self.scaled(dt, e_hh, e_mn, scale)
            ])
        except IndexError:
            return None

    def infos(self):
        return self.uid, self.obj_type, self.x, self.y, self.z
