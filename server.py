import asyncio
import random
import struct
from asyncio import StreamWriter, StreamReader, sleep
import time
import datetime

from human.base import HumanBase
from human.iterator import Humans

# HOST = '192.168.1.32'
HOST = 'localhost'
PORT = 9696

"""
start
   81865
     +-------------
     |
         
   08:22
   
   
   loop 
     Humans
     next_transition()
     
       -> si aucune transition :
           -> liste des transitions à venir avec  t. début < now < t. fin
           -> on en tire une au hasard
           -> on la renvoie 
        
           
           A  changement d'état dans une intervalle
           

        AT_HOME
        entre 08h00 et 08h30  -> transition = GOTO_WORK
                                 durée     = 10s
        -> AT_WORK 
        
        entre 17h45 et 18h00  -> transition = GOTO_HOME
                                 durée     = 10s
        -> AT_HOME
"""

humans = Humans([
    HumanBase(i, 0, random.random() * 20, 0.0, random.random() * 20)
    for i in range(random.randint(5000, 5000))
])


async def handle(reader: StreamReader, writer: StreamWriter) -> None:
    WAIT_ASK_MS = 5000  # 1000ms = 1s
    # all errors
    RESULT_OK = 0
    RESULT_ERROR_WAIT = 1
    wait_ask_ms = WAIT_ASK_MS

    async def _read(struct_str):
        try:
            result = await reader.read(struct.calcsize(struct_str))
            if len(result) == 0:
                return result, False
        except ConnectionResetError:
            return b'', False
        return result, True

    def close_and_return(msg: str = None) -> None:
        if msg:
            print(msg)
        writer.close()
        return None

    MAX_U_LONG = 2 ** 63 - 1
    print(f"Connected with {writer.get_extra_info('peername')!r}")
    # Message #1 sent from client = bool for little or big endian
    # help(struct) says:
    # '<': little-endian, std. size & alignment
    # '>': big-endian, std. size & alignment
    await sleep(1)
    a_bool, ok = await _read('c')
    if not ok:
        return close_and_return('? Socket closed unexpectedly ?')
    is_little_endian, = struct.unpack_from('?', a_bool)
    is_le = '<' if is_little_endian else '>'

    secure_random_number = random.randint(0, MAX_U_LONG)
    # ! find a way to write time as unsigned long. For now, just as string:
    start_time_ms = time.time()  # not in ms right now, I do it later
    dt = datetime.datetime.fromtimestamp(start_time_ms)
    # str.encode() convert a string to a byte string
    dt = str.encode(dt.strftime('%Y-%m-%d %H:%M:%S'))

    # let's put start_time in ms:
    start_time_ms = int(start_time_ms * 1000)

    # [ long ] [ string  ] [ long         ] [ long ] [ (id, .., x, y, z) x nb ]
    # [secure] [date time] [delay next ask] [nb obj] [ (obj) x nb             ]
    flattened_humans = humans.flattened()

    # "Q":unsigned long long -> 64 bits, "L" = unsigned long -> only 32 bits!
    # "f": float, h:short; H:unsigned short:
    str_pack = f"{is_le}Q19sHQ{'QQfff' * len(humans)}"
    buffer = bytearray(struct.calcsize(str_pack))
    struct.pack_into(
        # format string, where to pack, start:
        str_pack, buffer, 0,
        # all other values to be packed:
        secure_random_number, dt, wait_ask_ms, len(humans), *flattened_humans
    )
    writer.write(buffer)
    await writer.drain()

    # region - main loop -
    """
    Message struct: 
    # [ long ] [ uint   ]          [ long ] [ (id, .., x, y, z) x nb ]
    # [secure] [ result ] / if ok: [nb obj] [ (obj) x nb             ]
    """
    while True:
        time.sleep(wait_ask_ms / 1000)
        data, ok = await _read(f'{is_le}Q')
        (secure_number_from_client, ) = struct.unpack_from(f'{is_le}Q', data, 0)
        if not ok:
            return close_and_return('? Socket closed unexpectedly ?')
        if secure_number_from_client != secure_random_number:
            return close_and_return("Got bad secure number. Closing.")

        secure_random_number = random.randint(0, MAX_U_LONG)
        if (time.time() * 1000) - start_time_ms < wait_ask_ms:
            # client asked too early -> wait
            str_pack = f"{is_le}QH"
            buffer = bytearray(struct.calcsize(str_pack))
            struct.pack_into(
                str_pack, buffer, 0,
                secure_random_number, RESULT_ERROR_WAIT
            )
        else:
            # ok, client waited enough
            str_pack = f"{is_le}QHQ"
            buffer = bytearray(struct.calcsize(str_pack))
            struct.pack_into(
                str_pack, buffer, 0,
                secure_random_number, RESULT_OK, 0  # nb humans
            )
            writer.write(buffer)
            await writer.drain()
            return close_and_return('Socket closed nicely.')
    # endregion - main loop -

    # region - reading sample -
    """ reading sample
    data = await reader.read(4096)
    message = data.decode('utf8')

    # having fun under Windows: speak!
    import pythoncom
    import win32com.client as win32_client
    pythoncom.CoInitialize()
    speak = win32_client.Dispatch('SAPI.SpVoice')
    speak.Speak(message)
    print(f"Received {message!r} from {addr!r}")
    print(f"Send: {message!r}")
    """
    # endregion


async def main() -> None:
    # print(time.gmtime())
    # print(time.localtime())

    server = await asyncio.start_server(handle, HOST, PORT)
    print(f'Serving on {server.sockets[0].getsockname()}')
    async with server:
        await server.serve_forever()


asyncio.run(main())
